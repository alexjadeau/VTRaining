var createBtn = document.getElementById("createBtn"),
    background = document.getElementById("background"),
    welcome = document.getElementById("welcome"),
    next = document.getElementById("next"),
    goBack = document.getElementById("goBack");

createBtn.addEventListener("click", function() {
    background.setAttribute(
        "style", "transition: all 0.2s; opacity: 0;"
    )
    welcome.setAttribute(
        "style", "transition: all 0.2s; opacity: 0; z-index: -1;"
    )
    setTimeout(function() {
        next.setAttribute(
            "style", "transition: all 0.2s; opacity: 1;"
        )
    }, 200);
});
goBack.addEventListener("click", function() {
    background.setAttribute(
        "style", "transition: all 0.2s; opacity: 1;"
    )
    next.setAttribute(
        "style", "transition: all 0.2s; opacity: 0;"
    )
    setTimeout(function() {
        welcome.setAttribute(
            "style", "transition: all 0.2s; opacity: 1; z-index: 1;"
        )
    }, 200);
});